#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

#include <TROOT.h>
#include <TApplication.h>
#include <TRint.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TH1D.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TMath.h>
#include <TDatime.h>
#include <TVector3.h>

#include "LHCfEvent.h"
#include "RHICfRaw_Op2017.h"
#include "A1Cal1.h"
#include "A1Cal2M.h"

#include "A1Calibration.h"
#include "A1Reconstruction.h"

int main(int argc, char **argv) {

	TString arg_runtype         = "";
	TString arg_inputfilename   = "";
	TString arg_outputfilename  = "";
	TString arg_pedefile        = "";
	TString arg_startiev	    = "";
	TString arg_endiev          = "";

	for (int i = 1; i < argc; i++) {

		TString ss = argv[i];

		if(ss=="-r" || ss=="--runtype"){
			arg_runtype = argv[++i];
			strcpy(argv[i], "");
		}
		if(ss=="-i" || ss=="--input"){
			arg_inputfilename = argv[++i];
			strcpy(argv[i], "");
		}
		if(ss=="-o" || ss=="--output"){
			arg_outputfilename = argv[++i];
			strcpy(argv[i], "");
		}
		if(ss=="-p" || ss=="--pedestal"){
			arg_pedefile = argv[++i];
			strcpy(argv[i], "");
		}
		if(ss=="-f"){
			arg_startiev = argv[++i];
			strcpy(argv[i], "");
		} 
		if(ss=="-t"){
			arg_endiev = argv[++i];
			strcpy(argv[i], "");
		}
	}
	if(arg_runtype=="" || arg_inputfilename=="" || arg_outputfilename=="" || arg_pedefile=="" || arg_startiev=="" || arg_endiev==""){

		cerr << "./dataReconstruction -r [run type] -i [input] -o [output] -p [pedestal] -f [start iev] -t [end iev]" << endl;
		return -1;
	}

	TString  inputfilename  = arg_inputfilename;
	TString  outputfilename = arg_outputfilename;
	TString  pedefile       = arg_pedefile;
	int	 startiev	= atoi(arg_startiev);
	int	 endiev		= atoi(arg_endiev);

	TRint theApp("App", &argc, argv, 0, 0, kTRUE);
	gROOT -> SetBatch(kTRUE);

	// A1Calibration.
	A1Calibration *calibration = new A1Calibration();
	calibration -> SetupDefault_RHICfOp2017();
	int length = inputfilename.Sizeof();
	TString runnumber(inputfilename(length-10, 4));
	TString avepedefile = "/ccj/u/mhkim/RHICf/analysis/Reconstruction/tables/average_pedestal/average_pedestal_run" + runnumber + ".dat";
	calibration -> SetPedestalFile(pedefile.Data(), avepedefile.Data());
	calibration -> Initialize();

	// A1Reconstruction.
	A1Reconstruction *reconstruction = new A1Reconstruction();
	if(arg_runtype=="TS" || arg_runtype=="TL" || arg_runtype=="TOP") reconstruction -> SetRunType(arg_runtype);
	reconstruction -> Initialize();

	// Input.
	LHCfEvent *ev   = new LHCfEvent();
	TChain *tree    = new TChain("LHCfEvents");
	tree -> AddFile(inputfilename.Data());
	tree -> SetBranchAddress("ev.", &ev);

	// Output.
	TFile     *ofile = new TFile(outputfilename, "RECREATE");
	LHCfEvent *oev   = new LHCfEvent();
	TTree     *otree = new TTree("LHCfEvents", "Collision Events");
	otree->Branch("ev.", "LHCfEvent", &oev);
	otree->SetMaxTreeSize(4000000000);

	// TCanvas scan.
	const int nscan = 10;
	TCanvas* scanCanv[3][3][nscan];
	for(int tshit=0;tshit<3;tshit++){
		for(int tlhit=0;tlhit<3;tlhit++){
			for(int iscan=0;iscan<nscan;iscan++){

				scanCanv[tshit][tlhit][iscan] = new TCanvas(Form("scanCanv_%d_%d_%d", tshit, tlhit, iscan), "", 800, 800);
			}
		}
	}

	gROOT->cd();

	int nevmax = tree->GetEntriesFast();
	int nevent = 0;;
	int nevent_a1raw = 0;
        int ntype1 = 0;
        int ntype2 = 0;
	bool NoEvent = false;

	A1Phys *phys = new A1Phys();
	A1Cal2M* cal2 = new A1Cal2M();
	A1Rec* rec = new A1Rec();
	RHICfCollection* col = new RHICfCollection();

	// ======================================= Start event loop. ======================================= //
	for (int iev=startiev; iev<endiev; iev++){

		Int_t ievlocal = tree->LoadTree(iev);
		if(ievlocal<0){
			if(iev==startiev) NoEvent = true;
			cout << iev << endl;
			break;
		}

		//continue;
		//cout << iev << endl;
		if (iev && iev % 200 == 0) { cerr << '*'; }
		if (iev && iev % 5000 == 0) { cerr << iev << endl; }
		ev->Delete();
		tree->GetEntry(iev);
		nevent++;
		if (!ev->Check("a1raw")){ continue; }

		// Calibration.
		calibration->Calculate(ev->Get("a1raw"));
		reconstruction->SetData(calibration->GetCal2());
		cal2 = calibration -> GetCal2();

		// Reconstruction.
		reconstruction->Reconstruct();
		oev -> HeaderCopy(ev);
                rec = reconstruction -> GetRec();
		col = reconstruction -> GetRHICfCol();
		oev -> Add(rec);
		oev -> Add(col);

		int TShit = rec -> GetResultNumberOfHits(0);
                int TLhit = rec -> GetResultNumberOfHits(1);
		if(TShit==1 && TLhit==1) ntype1++;
                if(TShit==0 && TLhit==2){
			//cout << "Type-II candidate" << endl;
			ntype2++;
		}

                int scanNum = reconstruction -> GetScanNum(TShit, TLhit);
		int tsmax = rec -> GetMaxBarLayer(0);
		int tlmax = rec -> GetMaxBarLayer(1);
                if(scanNum < nscan) reconstruction -> GetScan(scanCanv[TShit][TLhit][scanNum], tsmax, tlmax, iev);

		otree->Fill();
		oev->Clear();
		nevent_a1raw++;
	}
	// ======================================= End of event loop. ======================================= //

	printf("ntype1 =  %d\n", ntype1);
	printf("ntype2 =  %d\n", ntype2);
	printf("nevent = %d\n", nevent);
	printf("nevent_a1raw = %d\n", nevent_a1raw);

	calibration -> Write();
	ofile->cd();
	ofile->Write("", TObject::kOverwrite);
	for(int tshit=0;tshit<3;tshit++){
		for(int tlhit=0;tlhit<3;tlhit++){
			for(int iscan=0;iscan<reconstruction->GetScanNum(tshit, tlhit);iscan++){

				scanCanv[tshit][tlhit][iscan] -> Write(Form("scan_%d_%d_%d", tshit, tlhit, iscan));
			}
		}
	}
	ofile->Close();

	char rmname[256];
	//sprintf(rmname, "%s", outputfilename);
	if(NoEvent) system(Form("rm %s", outputfilename.Data()));

	theApp.Run();
	return 0;
}
