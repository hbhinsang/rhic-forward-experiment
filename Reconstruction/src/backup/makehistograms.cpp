/**********************************************************************\
  Making histograms from Quick Reconstruction results
  for RHICf Operation 2017
  \**********************************************************************/

#include <iostream>
#include <fstream>
#include <iomanip>

using namespace std;

#include <TROOT.h>
#include <TApplication.h>
#include <TRint.h>
#include <TCanvas.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TFile.h>
#include <TStyle.h>
#include <TString.h>
#include <TMath.h>
#include <TGraphErrors.h>
#include <TF1.h>
#include <TF2.h>
#include <TVector3.h>

#include <LHCfEvent.h>
#include <A1Rec1.h>
#include <A1Phys.h>

typedef A1Rec1 A1Rec;

class MakeHistograms {

	public:
		TFile *ofile;

		TH1D* h1_eventCounter[5];
		// [0]: 0: Shower Trig, 1: Pi0 Trig, 2: High EM Trig.
		// [1]: 0: Neutron-like, 1: Single photon-like, 2: Type1 pi0, 3: Type2 pi0 in pi0 Trig.
		// [2]: Same with [1] in high EM Trig.
		TH1D* h1_energy[5];// Same with h1_eventcounter[2] with all Trig.
		TH2D* h2_hitmap[5];// [0]: Neutron-like, [1]: Photon-like.
		TH1D* h1_pi0[5];// Invariant mass plot, [0]: Type1, [1]: Type2 TS, [2]: Type2 TL.

	public:
		MakeHistograms(const char *filename);

		~MakeHistograms();

		void Write();

		int Fill(A1Phys *phys, A1Rec *rec);
		int Fill(A1Phys *phys, A1Rec *rec, int trg);
		int FillPi0(A1Phys *phys, A1Rec* rec, int type, int tower);
};

MakeHistograms::MakeHistograms(const char *filename) {

	// Open File
	ofile = new TFile(filename, "RECREATE");

	for(int i=0;i<5;i++){

		h1_eventCounter[i] = new TH1D(Form("h1_eventCounter_%d", i), "", 10, 0, 10);
		h2_hitmap[i] = new TH2D(Form("h2_hitmap_%d", i), "", 100, -50, 50, 100, -20, 80);
		h1_energy[i] = new TH1D(Form("h1_energy_%d", i), "", 500, 0, 500);
		h1_pi0[i] = new TH1D(Form("h1_pi0_%d", i), "", 300, 0, 300);
	}

	gROOT->cd();
	return;
}

MakeHistograms::~MakeHistograms() {

	ofile->Close();
}

void MakeHistograms::Write() {

	ofile->cd();
	ofile->Write();
	gROOT->cd();
}

int MakeHistograms::Fill(A1Phys *phys, A1Rec *rec) {

	Fill(phys, rec, 0); // All

	if ((phys->flag[0] & 0x010)) { Fill(phys, rec, 1); } // Shower Trig.
	if ((phys->flag[0] & 0x080)) { Fill(phys, rec, 2); } // Pi0 Trig.
	if ((phys->flag[0] & 0x200)) { Fill(phys, rec, 3); } // High EM Trig.r
	if ((phys->flag[0] & 0x020)) { Fill(phys, rec, 4); } // Pedestal Trigger (Zero bias trigger )
	if ((phys->flag[0] & 0x010) && (phys->flag[0] & 0x080)) { Fill(phys, rec, 5); } // Shower Trigger & Special1 Trigger
	if ((phys->flag[0] & 0x010) && ((phys->flag[1] & 0x8) || (phys->flag[1] & 0x80000))) { Fill(phys, rec, 6); } // Shower Trigger & Special1 Trigger

	return 0;
}

int MakeHistograms::Fill(A1Phys *phys, A1Rec *rec, int trg) {

	if(1<=trg && trg<=3) h1_eventCounter[0] -> Fill(trg-1);

	for(int itower=0;itower<2;itower++){

		if(phys->IsHadron(itower) && phys->Hits(itower)==1){// Neutron-like

			h1_energy[0] -> Fill(phys->Energy(itower));
			
			if(phys->Energy(itower)>50){

				if(trg==2||trg==3) h1_eventCounter[trg-1] -> Fill(0);
				h2_hitmap[0] -> Fill(phys->GPosX_Detector(itower), phys->GPosY_Detector(itower));
			}
		}

		if(phys->IsElemag(itower) && phys->Hits(itower)==1){// Single photon-like

			h1_energy[1] -> Fill(phys->Energy(itower));

			if(phys->Energy(itower)>25){

				if(trg==2||trg==3) h1_eventCounter[trg-1] -> Fill(1);
                                h2_hitmap[1] -> Fill(phys->GPosX_Detector(itower), phys->GPosY_Detector(itower));
			}
		}
	}

	if(phys->IsElemag(0) && phys->Hits(0)==1 && phys->Energy(0)>25 && phys->IsElemag(1) && phys->Hits(1)==1 && phys->Energy(1)>25){// Type1 pi0.

		if(trg==2||trg==3) h1_eventCounter[trg-1] -> Fill(2);
		h1_energy[2] -> Fill(phys->Energy(0) + phys->Energy(1));
		FillPi0(phys, rec, 1, -1);
	}

	if((phys->IsElemag(0) && phys->Hits(0)==2 && phys->Energy(0)>50) || (phys->IsElemag(1) && phys->Hits(1)==2 && phys->Energy(1)>50)){// Type2 pi0.

		if(trg==2||trg==3) h1_eventCounter[trg-1] -> Fill(3);

		if(phys->IsElemag(0) && phys->Hits(0)==2 && phys->Energy(0)>50){

			h1_energy[3] -> Fill(phys->Energy(0));
			FillPi0(phys, rec, 2, 0);
		}

		if(phys->IsElemag(1) && phys->Hits(1)==2 && phys->Energy(1)>50){

			h1_energy[3] -> Fill(phys->Energy(1));
			FillPi0(phys, rec, 2, 1);
		}
	}

	return 0;
}

int MakeHistograms::FillPi0(A1Phys *phys, A1Rec* rec, int type, int tower) {

	static const double z = 18000;

	if(type==1 && phys->InFiducial(0) && phys->InFiducial(1)){

		double x1 = phys->GPosX_Detector(0);
		double y1 = phys->GPosY_Detector(0);
		double r1 = sqrt(pow(x1,2) + pow(y1,2) + pow(z,2));
		double x2 = phys->GPosX_Detector(1);
		double y2 = phys->GPosY_Detector(1);
		double r2 = sqrt(pow(x2,2) + pow(y2,2) + pow(z,2));

		double E1 = phys->Energy(0)*1000;
		double E2 = phys->Energy(1)*1000;
		TVector3 p1 = TVector3((x1/r1)*E1, (y1/r1)*E1, (z/r1)*E1);
		TVector3 p2 = TVector3((x2/r2)*E2, (y2/r2)*E2, (z/r2)*E2);

		double E = E1+E2;
		TVector3 p = p1+p2;
		
		h1_pi0[0] -> Fill(sqrt(pow(E,2) - pow(p.X(),2) - pow(p.Y(),2) - pow(p.Z(),2)));
	}

	if(type==2){


	}

	return 0;
}

// ---------------------------------------------------------------------
//                                  MAIN
// ---------------------------------------------------------------------
int main(int argc, char **argv) {

	TString inputfilename  = "";
	TString outputfilename = "";

	for (int i = 1; i < argc; i++) {
		string ss = argv[i];

		if (ss == "-i" || ss == "--input") {
			inputfilename = argv[++i];
			strcpy(argv[i], "");
		}
		if (ss == "-o" || ss == "--output") {
			outputfilename = argv[++i];
			strcpy(argv[i], "");
		}
	}

	if (inputfilename == "" || outputfilename == "") {
		cerr << "give an input file name and/or an output file name." << endl;
		return -1;
	}

	// ++++++ INITIALIZATIONS +++++++++++++++++++
	TRint theApp("App", &argc, argv, 0, 0, kTRUE);

	// ++++++ OPEN DATA FILE ++++++++++++++++++++++++++
	LHCfEvent *ev   = new LHCfEvent();
	TChain    *tree = new TChain("LHCfEvents");
	TString repeatfilename = "";
	TString jjeomroot = ".root";	

	for(int inum=0;inum<=25;inum++){

		repeatfilename.Form("%s%d%s", inputfilename.Data(), inum, jjeomroot.Data());
		tree -> AddFile(repeatfilename.Data());
	}

	tree->SetBranchAddress("ev.", &ev);

	//+++++ OPEN OUTPUT FILE +++++++++++++++++++++++++

	MakeHistograms *hists = new MakeHistograms(outputfilename.Data());

	// +++++ EVENT LOOP ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	int      nevmax = tree->GetEntriesFast();
	int      nevent = 0;

	A1Phys   *phys  = new A1Phys();

	//  == START OF EVENT LOOP ==
	for (int iev    = 0; iev < nevmax; iev++) {

		Int_t ievlocal = tree->LoadTree(iev);
		if (ievlocal < 0) break;

		if (iev && iev % 1000 == 0) { cerr << '*'; }
		if (iev && iev % 20000 == 0) { cerr << iev << endl; }
		ev->Delete();
		tree->GetEntry(iev);

		if (!ev->Check("a1rec")) { continue; }
		nevent++;

		A1Rec *rec = (A1Rec *) ev->Get("a1rec");

		rec->FillToPhys(phys);

		hists -> Fill(phys, rec);
	}

	gROOT -> SetBatch(kTRUE);

	cerr << endl;

	hists->Write();

	//theApp.Run();

	return 0;
}
