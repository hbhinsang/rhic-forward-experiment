//
// Created by MENJO Hiroaki on 2017/05/24.
//

#include "../include/A1Reconstruction.h"

#include <iostream>
#include <iomanip>

using namespace std;

#include <TMath.h>

A1Reconstruction::A1Reconstruction() {
    // Initialize();
}


void A1Reconstruction::Initialize() {
    fCal    = new A1Cal2M();
    fCalOrg = new A1Cal2M();
    fRec    = new A1Rec("a1rec", "");
    
    // Parameters
    TString dirLibrary = LIBDIR;          // LIBDIR is defined in Makefile
    TString dirWork    = WORKDIR;
    paramHitPeakThreshold    = 0.005;    // Not optimized yet
    paramHitdEThreshold      = 0.05;     // Not optimized yet
    paramSumdEThreshold      = 0.3;     // Not optimized yet
    paramGSOLY20mm           = dirWork + "/tables/151004_dEratio_map_20mm_verTH2D.root";
    paramGSOLY40mm           = dirWork + "/tables/151021_dEratio_map_40mm_verTH2D.root";
    paramShowerLeakagePhoton = dirLibrary + "/tables/leakage_arm1_081118.root"; // only for leak-out
    paramShowerLeakageHadron = dirLibrary + "/tables/leakage_arm1_081118.root"; // Not optimized yet
    
    // Table of Scintillator Light Yield
    fPosdep = new ScintiPosDepGSO();
    fPosdep->SetdEMapArm1((char *) paramGSOLY20mm.Data(), (char *) paramGSOLY40mm.Data());
    
    // Table of Shower Leakage
    fLeakPhoton = new ConShowerLeakage();
    fLeakPhoton->ReadROOTFile((char *) paramShowerLeakagePhoton.Data());
    fLeakHadron = new ConShowerLeakage();
    fLeakHadron->ReadROOTFile((char *) paramShowerLeakageHadron.Data());
    
    // L20, L90 Calculation
    fPid = new ParticleID01();
    
    return;
}


//////////////////////////////////////////////////////////////////////////////////
////                      FUNCTIONS FOR RECONSTRUCTION                        ////
//////////////////////////////////////////////////////////////////////////////////

int A1Reconstruction::Reconstruct() {
    
    // Reset
    fRec->clear();
    fRec->resize();
    
    // Fill Event Information
    FillEventInfo();
    
    // Reconstruction of PID
    ReconstructPID();
    
    // Reconstruction of Hit Position
    ReconstructHitPosition();
    
    // Reconstruction of Energy
    ReconstructEnergy();
    
    return 0;
}

int A1Reconstruction::FillEventInfo() {
    
    // Fill the event header
    fRec->run     = fCal->run;
    fRec->number  = fCal->number;
    fRec->gnumber = fCal->gnumber;
    fRec->time[0] = fCal->time[0];
    fRec->time[1] = fCal->time[1];
    
    for (int i = 0; i < 3; i++) {
        fRec->SetFlags(i, fCal->flag[i]);
    }
    
    fRec->SetEventQuality(A1Rec::GOOD); // For the moment, all events are set to GOOD
    
    // Dummy Bunch Information (1,2 are yellow, blue beams but no information about beam direction in RHICf)
    fRec->SetBunchNumber(1, fCal->counter[27]);
    fRec->SetBunchNumber(2, fCal->counter[27]);
    
    // Fill Counter values
    for (int i = 0; i < 35; i++) {
        fRec->SetCounter(i, fCal->counter[i] + 1.E-5); // add 1.E-5 for double values
    }
    
    return 0;
}

int A1Reconstruction::ReconstructPID() {
    // calculate L20 and L90, even no hit in the tower
    
    double l20s, l20l, l90s, l90l;
    
    fPid->Calculate(fCal, 0);
    l20s = fPid->GetL20(0);
    l90s = fPid->GetL95(0); // L90
    fPid->Calculate(fCal, 1);
    l20l = fPid->GetL20(1);
    l90l = fPid->GetL95(1); // L90
    fRec->SetL20(0, l20s);
    fRec->SetL20(1, l20l);
    fRec->SetL90(0, l90s);
    fRec->SetL90(1, l90l);
    
    // Fill Recommended PID result
    for (int itower = 0; itower < 2; itower++) {
        if (fRec->GetL90(itower) < PIDThresholdPhoton(itower, 0.))
            fRec->SetResultPID(itower, A1Phys::ELEMAG);
        else
            fRec->SetResultPID(itower, A1Phys::HADRON);
    }
    
    return 0;
}

int A1Reconstruction::ReconstructHitPosition() {
    // This is for quick analysis
    // The hit position is calculated by the weighted average method for peak +1/-1 bins with the power index of 3.
    // See  SearchHitPosition
    // The criteria for hit decision in this function
    //   1. Max dE at GSO bars in the layer > paramHitPeakThreshold
    //   2. Max dE of the GSO plate facing the GSO bar hodoscopes > paramHitdEThreshold
    
    // Hit position search for each layer and xy
    for (int tower = 0; tower < 2; ++tower) {
        for (int layer = 0; layer < 4; ++layer) {
            for (int xy = 0; xy < 2; ++xy) {
                int    pos;
                double center, maxvalue;
                if (tower == 0) {
                    center   = SearchHitPositionFast(20, fCal->scifi0[layer][xy], pos);
                    maxvalue = fCal->scifi0[layer][xy][pos];
                } else {
                    center   = SearchHitPositionFast(40, fCal->scifi1[layer][xy], pos);
                    maxvalue = fCal->scifi1[layer][xy][pos];
                }
                // Fill the results to Rec
                fRec->SetHitPosition(tower, layer, xy, center);
                fRec->SetPeakPosition(tower, layer, xy, pos);
                fRec->SetdEAtPeak(tower, layer, xy, maxvalue);
                
                // Check the peak value for hit decision
                if (maxvalue > paramHitPeakThreshold)
                    fRec->SetNumberOfHits(tower, layer, xy, 1);
                else
                    fRec->SetNumberOfHits(tower, layer, xy, 0);
            }
        }
    }
    
    // Reconstruct shower hit position form the all four layer's results.
    // Take the hit position of the Max dE layer
    for (int tower = 0; tower < 2; ++tower) {
        const int platelayer[4] = {2, 4, 12, 15};
        double    platedE[4];
        int       maxlayer;
        
        for (int i = 0; i < 4; ++i) platedE[i] = fCal->cal[tower][platelayer[i]];
        maxlayer = TMath::LocMax(4, platedE);
        if (platedE[maxlayer] > paramHitdEThreshold &&
            fRec->GetNumberOfHits(tower, maxlayer, 0) &&
            fRec->GetNumberOfHits(tower, maxlayer, 1)) {
            fRec->SetResultNumberOfHits(tower, 1);
            fRec->SetResultHitPosition(tower, 0, fRec->GetHitPosition(tower, maxlayer, 0)); // x
            fRec->SetResultHitPosition(tower, 1, fRec->GetHitPosition(tower, maxlayer, 1)); // y
        } else {
            fRec->SetResultNumberOfHits(tower, 0);
            fRec->SetResultHitPosition(tower, 0, 0); // x
            fRec->SetResultHitPosition(tower, 1, 0); // y
        }
    }
    
    return 0;
}

int A1Reconstruction::ReconstructEnergy() {
    // Hit decision by using sum dE
    for (int tower = 0; tower < 2; ++tower) {
        double sumdE = fCal->calsum2(tower, 0, 15);
        fRec->SetSumdE(tower, sumdE);
        fRec->SetSumdE2(tower, fCal->calsum2(tower, 1, 12));
        if (fRec->GetResultNumberOfHits(tower) > 0 && sumdE < paramSumdEThreshold)
            fRec->SetResultNumberOfHits(tower, 0);
    }
    
    // For Photons ============================================================
    
    // Light Yield & Shower Leakage-out corrections
    CorrectionLightYieldPhoton();
    
    // Energy Reconstruction for Double Tower Event
    if (fRec->GetResultNumberOfHits(0) > 0 && fRec->GetResultNumberOfHits(1) > 0)
        ReconstructEnergyPhotonDouble();
    
    // Energy Reconstruction for Single Tower Event in Small Tower
    if (fRec->GetResultNumberOfHits(0) > 0 && fRec->GetResultNumberOfHits(1) == 0)
        ReconstructEnergyPhotonSingle(0);
    
    // Energy Reconstruction for Sinelge Tower Event in Large Tower
    if (fRec->GetResultNumberOfHits(0) == 0 && fRec->GetResultNumberOfHits(1) > 0)
        ReconstructEnergyPhotonSingle(1);
    
    
    // For Hadrons ============================================================
    fCal->copydata(fCalOrg, A1Cal2::CAL); // Reset the LY correction
    
    for (int tower = 0; tower < 2; ++tower) {
        if (fRec->GetResultNumberOfHits(tower) == 0) continue;
        ReconstructEnergyHadron(tower);
    }
    
    return 0;
}

int A1Reconstruction::CorrectionLightYieldPhoton() {
    double   x, y;
    for (int tower = 0; tower < 2; tower++) {
        // Only for towers with #hits>0, the correction is applyed.
        if (fRec->GetResultNumberOfHits(tower) > 0) {
            x = fRec->GetResultHitPosition(tower, 0);
            y = fRec->GetResultHitPosition(tower, 1);
            fPosdep->Calibration(fCal, tower, x, y);
        }
    }
    
    return 0;
}

double A1Reconstruction::CalculateEnergyPhotonSimple(int tower) {
    // Simple energy Reconstruction for hit decision
    // Only shower leakage-out correction is applied
    
    if (fRec->GetResultNumberOfHits(tower) == 0) {
        return -1.;
    }
    
    double sum, sumc, x, y;
    x    = fRec->GetResultHitPosition(tower, 0);
    y    = fRec->GetResultHitPosition(tower, 1);
    sum  = fCal->calsum2(tower, 1, 12);
    sumc = sum;
    
    return EnergyConversionPhoton(tower, sumc);
}

int A1Reconstruction::ReconstructEnergyPhotonSingle(int tower) {
    // Energy Reconstruction for Single Tower Hit Events
    
    const int tower_nohit = (tower + 1) % 2;
    double    x, y;
    x = fRec->GetResultHitPosition(tower, 0);
    y = fRec->GetResultHitPosition(tower, 1);
    
    // Copy cal for non hit tower
    for (int il = 0; il < 16; il++) {
        fCal->cal[tower_nohit][il] = fCalOrg->cal[tower_nohit][il];
    }
    
    
    // Shower leakage in/out correction from only the hit tower.
    double sum[2], sumc[2];
    sum[tower]        = fCal->calsum2(tower, 1, 12);
    sumc[tower]       = sum[tower];
    sum[tower_nohit]  = fCal->calsum2(tower_nohit, 1, 12);
    sumc[tower_nohit] = sum[tower_nohit] - sumc[tower] * fLeakPhoton->GetLeakinFactor(1, tower, x, y);
    
    // Fill Results in A1Rec
    for (int tower = 0; tower < 2; tower++) {
        fRec->SetSumdE(tower, fCal->calsum2(tower, 0, 15));
        fRec->SetSumdE2(tower, sumc[tower]);
        fRec->SetResultEnergy(tower, A1Phys::ELEMAG, EnergyConversionPhoton(tower, sumc[tower]));
        // For the moment, even in hadron events the same conversion function is applied.
        // rec->SetResultEnergy(tower,A1Phys::HADRON, EnergyConversion(tower,sumc[tower]));
    }
    return 0;
}

int A1Reconstruction::ReconstructEnergyPhotonDouble() {
    // Energy Reconstruction for Double Tower Hit Events
    
    double   x[2], y[2];
    double   sum[2], sumc[2];
    double   lo[2], li[2], det;
    for (int tower = 0; tower < 2; tower++) {
        x[tower] = fRec->GetResultHitPosition(tower, 0);
        y[tower] = fRec->GetResultHitPosition(tower, 1);
        
        sum[tower] = fCal->calsum2(tower, 1, 12);
        //    lo[tower]  = fLeakPhoton->GetFactor(1,tower,x[tower],y[tower]);
        lo[tower]  = 1.0; // makino 10.10.2015
        li[tower]  = fLeakPhoton->GetLeakinFactor(1, tower, x[tower], y[tower]);
    }
    
    // Shower leakage in/out correction for the both towers
    det = 1. / (lo[0] * lo[1] - li[0] * li[1]);
    sumc[0] = det * (sum[0] * lo[1] - sum[1] * li[0]);
    sumc[1] = det * (-1. * sum[0] * li[1] + sum[1] * lo[0]);
    
    // Fill Results in A1Rec
    for (int tower = 0; tower < 2; tower++) {
        fRec->SetSumdE(tower, fCal->calsum2(tower, 0, 15));
        fRec->SetSumdE2(tower, sumc[tower]);
        fRec->SetResultEnergy(tower, A1Phys::ELEMAG, EnergyConversionPhoton(tower, sumc[tower]));
    }
    
    return 0;
}

int A1Reconstruction::ReconstructEnergyHadron(int tower) {
    
    double sum, sumc, x, y;
    x    = fRec->GetResultHitPosition(tower, 0);
    y    = fRec->GetResultHitPosition(tower, 1);
    sum  = fCal->calsum2(tower, 2, 15);
    sumc = sum; // Shower leakage correction must be implemented here
    
    fRec->SetResultEnergy(tower, A1Phys::HADRON, EnergyConversionHadron(tower, sumc));
    
    return 0;
}


//////////////////////////////////////////////////////////////////////////////////
////                      FUNCTIONS GIVING PARAMETERS                         ////
//////////////////////////////////////////////////////////////////////////////////
double A1Reconstruction::EnergyConversionPhoton(int tower, double sumdE) {
    // [NOT FINALIZED YET]
    // From Suzuki's old study with old simulation by EPICS library.
    
    if (tower == 0)
        return 1.1527 + 0.023877 / 0.744E-3 * sumdE;
    if (tower == 1)
        return 0.9585 + 0.020527 / 0.744E-3 * sumdE;
    
    // Function used in LHCf analysis
//    if (tower == 0)
//        return 5.55112e-15 * sumdE * sumdE + 30.0259 * sumdE + 7.53553;
//    if (tower == 1)
//        return 5.55112e-14 * sumdE * sumdE + 26.3253 * sumdE + 5.0172;
    
    return 0.;
}

double A1Reconstruction::EnergyConversionHadron(int tower, double sumdE) {
    // [NOT FINALIZED YET]
    // Taken from Ueno's Master thesis.
    // Copy the one for TS to for TL
    
    if (tower == 0)
        return  88.9* sumdE + 10.7;
    if (tower == 1)
        return  88.9* sumdE + 10.7;
    
    return 0.;
}

double A1Reconstruction::PIDThresholdPhoton(int tower, double energy) {
    // [NOT OPTIMIZED YET]
    return 20.;
}

double A1Reconstruction::PIDThresholdHadron(int tower, double energy) {
    // [NOT OPTIMIZED YET]
    return 20.;
}

double A1Reconstruction::SearchHitPositionFast(int npos, double *values, int &pos) {
    
    int    maxpos = TMath::LocMax(npos, values);
    double weight = 0., center = 0.;
    
    if (maxpos >= 1 && maxpos <= npos - 2) {
        for (int p = maxpos - 1; p <= maxpos + 1; p++) {
            weight += TMath::Power(TMath::Abs(values[p]), 3);
            center += TMath::Power(TMath::Abs(values[p]), 3) * (0.5 + p);
        }
        center     = center / weight;
    } else {
        center = 0.5 + maxpos;
    }
    
    pos = maxpos;
    
    return center;
}








